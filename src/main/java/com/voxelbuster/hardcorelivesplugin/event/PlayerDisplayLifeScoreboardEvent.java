package com.voxelbuster.hardcorelivesplugin.event;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class PlayerDisplayLifeScoreboardEvent extends PlayerEvent {
    private static final HandlerList handlers = new HandlerList();

    public PlayerDisplayLifeScoreboardEvent(Player who) {
        super(who);
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}
