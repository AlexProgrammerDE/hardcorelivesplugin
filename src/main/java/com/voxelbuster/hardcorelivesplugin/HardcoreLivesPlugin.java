package com.voxelbuster.hardcorelivesplugin;

import com.voxelbuster.hardcorelivesplugin.command.PluginBaseCommand;
import com.voxelbuster.hardcorelivesplugin.event.PluginEventHandler;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class HardcoreLivesPlugin extends JavaPlugin {

    private static final HardcoreLivesPlugin instance;
    private static Economy econ = null;
    private static Permission perms = null;

    static {
        instance = (HardcoreLivesPlugin) Bukkit.getPluginManager().getPlugin("Hardcorelivesplugin");
        assert instance != null;
    }

    protected final Logger log = this.getLogger();
    private final File dataFolder = this.getDataFolder();
    private final File playersDir = new File(Paths.get(dataFolder.toPath().toString(), "players").toString());
    private final File configFile = new File(Paths.get(dataFolder.toString(), "config.json").toString());
    private final ConfigManager configManager = new ConfigManager();
    private final SaveTaskScheduler saveTaskScheduler = new SaveTaskScheduler(this);
    private final PluginEventHandler eventHandler = new PluginEventHandler(configManager, playersDir, this);
    private Objective scoreboardObjective;
    private boolean enabledBefore;
    private Scoreboard scoreboard;

    public static HardcoreLivesPlugin getInstance() {
        return instance;
    }

    public static Economy getEcon() {
        return econ;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        // Replaced by PluginBaseCommand.onCommand();
        /*if (command.getName().equalsIgnoreCase("hardcorelives")) {
            if (args.length < 1) {
                sender.sendMessage(new String[]{
                        ChatColor.AQUA + "/hl lives",
                        ChatColor.AQUA + "/hl scoreboard",
                        ChatColor.AQUA + "/hl lives buy " + ChatColor.GREEN + "<number>" +
                                ((configManager.getConfig().allowBuyingLives()) ? "" : ChatColor.RED + " : DISABLED"),
                        ChatColor.AQUA + "/hl lives sell " + ChatColor.GREEN + "<number>" +
                                ((configManager.getConfig().allowSellingLives()) ? "" : ChatColor.RED + " : DISABLED"),
                        ChatColor.AQUA + "/hl lives give " + ChatColor.GREEN + "<player> <number>" +
                                ((configManager.getConfig().allowGivingLives()) ? "" : ChatColor.RED + " : DISABLED"),
                        ChatColor.AQUA + "/hl scoreboardSlot " + ChatColor.GREEN + "<SIDEBAR/PLAYER_LIST/BELOW_NAME>" +
                                ChatColor.WHITE + " : Admin Only",
                        ChatColor.AQUA + "/hl lives " + ChatColor.GREEN + "<player>" + ChatColor.WHITE + " : Admin " +
                                "Only",
                        ChatColor.AQUA + "/hl resetAll " + ChatColor.WHITE + " : Admin Only",
                        ChatColor.AQUA + "/hl reset " + ChatColor.GREEN + "<player>" + ChatColor.WHITE + " : Admin " +
                                "Only",
                        ChatColor.AQUA + "/hl reload " + ChatColor.WHITE + " : Admin Only",
                        ChatColor.AQUA + "/hl config " + ChatColor.GREEN + "<key> <value>" + ChatColor.WHITE + " : " +
                                "Admin Only",
                        ChatColor.AQUA + "/hl setLives " + ChatColor.GREEN + "<player> <lives>" + ChatColor.WHITE +
                                " : Admin Only",
                        });
                return true;
            } else if (args.length == 1 && args[0].equalsIgnoreCase("lives")) {
                if (sender instanceof Player) {
                    Player p = (Player) sender;
                    ConfigManager.PlayerData data = configManager.getPlayerData(p);
                    if (data.getLives() == 1) {
                        p.sendMessage(
                                ChatColor.YELLOW + String.format("You have %d more life remaining.", data.getLives()));
                    } else {
                        p.sendMessage(
                                ChatColor.YELLOW + String.format("You have %d more lives remaining.", data.getLives()));
                    }
                } else {
                    sender.sendMessage(ChatColor.RED + "You must be a player to use this command!");
                }
                return true;
            } else if (args.length == 2 && args[0].equalsIgnoreCase("lives")) {
                if (PermissionUtil.hasPermission(sender, "hardcorelives.lives.other")) {
                    Player p = getServer().getPlayer(args[1]);
                    if (p == null) {
                        sender.sendMessage(ChatColor.RED + "Player not found.");
                    } else {
                        ConfigManager.PlayerData data = configManager.getPlayerData(p);
                        if (data.getLives() == 1) {
                            sender.sendMessage(
                                    ChatColor.YELLOW + String
                                            .format("%s has %d more life remaining.", p.getName(), data.getLives()));
                        } else {
                            sender.sendMessage(
                                    ChatColor.YELLOW + String
                                            .format("%s has %d more lives remaining.", p.getName(), data.getLives()));
                        }
                    }
                } else {
                    sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                }
                return true;
            } else if (args.length == 3 && args[0].equalsIgnoreCase("setlives")) {
                if (PermissionUtil.hasPermission(sender, "hardcorelives.setLives")) {
                    Player p = getServer().getPlayer(args[1]);
                    if (p == null) {
                        sender.sendMessage(ChatColor.RED + "Player not found.");
                    } else {
                        ConfigManager.PlayerData data = configManager.getPlayerData(p);
                        data.setLives(Integer.parseInt(args[2]));
                        getServer().getPluginManager().callEvent(new PlayerLifeCountChangedEvent(p, data.getLives()));
                        updateScoreboard();
                    }
                } else {
                    sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                }
                return true;
            } else if (args.length == 1 && args[0].equalsIgnoreCase("resetall")) {
                if (PermissionUtil.hasPermission(sender, "hardcorelives.resetAll")) {
                    try {
                        configManager.wipePlayers(playersDir);
                        sender.sendMessage(ChatColor.LIGHT_PURPLE + "All Hardcore Lives player data wiped.");
                        updateScoreboard();
                    } catch (IOException e) {
                        log.severe(ChatColor.RED + "Could not wipe players!");
                        e.printStackTrace();
                    }
                } else {
                    sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                }
                return true;
            } else if (args.length == 2 && args[0].equalsIgnoreCase("reset")) {
                if (PermissionUtil.hasPermission(sender, "hardcorelives.reset")) {
                    String playerName = args[1];
                    Player p = this.getServer().getPlayer(playerName);
                    if (p != null) {
                        configManager.setPlayerData(p, new ConfigManager.PlayerData(p, configManager.getConfig()));
                        sender.sendMessage(
                                ChatColor.LIGHT_PURPLE + "Hardcore Lives player " + p.getName() + " data wiped.");
                    } else {
                        sender.sendMessage(ChatColor.RED + "Player not found.");
                    }
                } else {
                    sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                }
            } else if (args.length == 1 && args[0].equalsIgnoreCase("reload")) {
                if (sender instanceof ConsoleCommandSender || PermissionUtil
                        .hasPermission(sender, "hardcorelives.reload")) {
                    sender.sendMessage(ChatColor.LIGHT_PURPLE + "Reloading HardcoreLives...");
                    reload(true);
                    sender.sendMessage(ChatColor.LIGHT_PURPLE + "Reload complete.");
                } else {
                    sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                }
                return true;
            } else if (args.length == 3 && args[0].equalsIgnoreCase("config")) {
                if (PermissionUtil.hasPermission(sender, "hardcorelives.config")) {
                    try {
                        switch (args[1]) {
                            case "allowTotemOfUndying":
                                configManager.getConfig().setAllowTotemOfUndying(Boolean.parseBoolean(args[2]));
                                break;
                            case "spectateWhenNoMoreLives":
                                configManager.getConfig().setSpectateWhenNoMoreLives(Boolean.parseBoolean(args[2]));
                                break;
                            case "startingLives":
                                configManager.getConfig().setStartingLives(Integer.parseInt(args[2]));
                                break;
                            case "allowBuyingLives":
                                configManager.getConfig().setAllowBuyingLives(Boolean.parseBoolean(args[2]));
                                break;
                            case "allowSellingLives":
                                configManager.getConfig().setAllowSellingLives(Boolean.parseBoolean(args[2]));
                                break;
                            case "allowGivingLives":
                                configManager.getConfig().setAllowGivingLives(Boolean.parseBoolean(args[2]));
                                break;
                            case "lifeBuyPrice":
                                configManager.getConfig().setLifeBuyPrice(Float.parseFloat(args[2]));
                                break;
                            case "lifeSellPrice":
                                configManager.getConfig().setLifeSellPrice(Float.parseFloat(args[2]));
                                break;
                            case "scoreboardDisplaySlot":
                                configManager.getConfig().setScoreboardDisplaySlot(DisplaySlot.valueOf(args[2]));
                                break;
                            default:
                                sender.sendMessage(ChatColor.RED + "Invalid config option.");

                        }
                        log.log(Level.INFO, "Saving config...");
                        try {
                            configManager.saveConfig(configFile);
                        } catch (IOException e) {
                            log.severe(ChatColor.RED + "Failed to save config!");
                            e.printStackTrace();
                        }
                    } catch (RuntimeException e) {
                        sender.sendMessage(ChatColor.RED + "Invalid argument.");
                    }
                } else {
                    sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                }
                return true;
            } else if (args.length == 1 && args[0].equalsIgnoreCase("scoreboard")) {
                if (!(sender instanceof Player)) {
                    sender.sendMessage(ChatColor.RED + "You must be a player.");
                    return true;
                }
                if (PermissionUtil.hasPermission(sender, "hardcorelives.scoreboard")) {
                    Player p = (Player) sender;
                    if (p.getScoreboard() != this.scoreboard) {
                        sender.sendMessage(ChatColor.GOLD + "Lives scoreboard on.");
                        p.setScoreboard(this.scoreboard);
                        getServer().getPluginManager().callEvent(new PlayerDisplayLifeScoreboardEvent(p));
                    } else {
                        sender.sendMessage(ChatColor.GOLD + "Lives scoreboard off.");
                        p.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
                    }
                } else {
                    sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                }
                return true;
            } else if (args.length == 3 && args[0].equalsIgnoreCase("lives") && args[1].equalsIgnoreCase("buy")) {
                if (!configManager.getConfig().allowBuyingLives()) {
                    sender.sendMessage(ChatColor.DARK_RED + "This command is disabled.");
                    sender.sendMessage(
                            ChatColor.YELLOW + "If you are an admin and this is incorrect, check your config values.");
                    return true;
                }
                if (!(sender instanceof Player)) {
                    sender.sendMessage(ChatColor.RED + "You must be a player.");
                    return true;
                }
                if (econ == null) {
                    sender.sendMessage(ChatColor.DARK_RED + "Economy not loaded.");
                    return true;
                }
                if (PermissionUtil.hasPermission(sender, "hardcorelives.lives.buy")) {
                    Player p = (Player) sender;
                    ConfigManager.PlayerData data = configManager.getPlayerData(p);

                    int numLives = Integer.parseInt(args[2]);
                    float buyprice = configManager.getConfig().getLifeBuyPrice();

                    if (numLives < 1) {
                        sender.sendMessage("You cannot buy less than 1 life.");
                        return true;
                    }

                    EconomyResponse response = econ.withdrawPlayer(p, buyprice * numLives);

                    if (response.type == EconomyResponse.ResponseType.SUCCESS) {
                        data.setLives(data.getLives() + numLives);
                        getServer().getPluginManager().callEvent(new PlayerLifeCountChangedEvent(p, data.getLives()));
                        p.sendMessage(ChatColor.GREEN + String.format("Purchased %d lives.", numLives));
                        updateScoreboard();
                    } else {
                        sender.sendMessage(ChatColor.RED + String.format("Insufficient funds. " +
                                        "The price for %d lives is %.2f. (%dx%.2f)", numLives, buyprice * numLives,
                                numLives,
                                buyprice));
                    }

                    if (data.getLives() == 1) {
                        p.sendMessage(
                                ChatColor.YELLOW + String.format("You have %d more life remaining.", data.getLives()));
                    } else {
                        p.sendMessage(
                                ChatColor.YELLOW + String.format("You have %d more lives remaining.", data.getLives()));
                    }
                } else {
                    sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                }
                return true;
            } else if (args.length == 3 && args[0].equalsIgnoreCase("lives") && args[1].equalsIgnoreCase("sell")) {
                if (!configManager.getConfig().allowSellingLives()) {
                    sender.sendMessage(ChatColor.DARK_RED + "This command is disabled.");
                    sender.sendMessage(
                            ChatColor.YELLOW + "If you are an admin and this is incorrect, check your config values.");
                    return true;
                }
                if (!(sender instanceof Player)) {
                    sender.sendMessage(ChatColor.RED + "You must be a player.");
                    return true;
                }
                if (econ == null) {
                    sender.sendMessage(ChatColor.DARK_RED + "Economy not loaded.");
                    return true;
                }
                if (PermissionUtil.hasPermission(sender, "hardcorelives.lives.sell")) {
                    Player p = (Player) sender;
                    ConfigManager.PlayerData data = configManager.getPlayerData(p);

                    int numLives = Integer.parseInt(args[2]);
                    float sellprice = configManager.getConfig().getLifeSellPrice();

                    if (numLives < data.getLives() && numLives > 0) {
                        econ.depositPlayer(p, sellprice * numLives);
                        data.setLives(data.getLives() - numLives);
                        getServer().getPluginManager().callEvent(new PlayerLifeCountChangedEvent(p, data.getLives()));
                        p.sendMessage(ChatColor.GREEN + String.format("Sold %d lives.", numLives));
                        updateScoreboard();
                    } else {
                        sender.sendMessage("You cannot sell more lives than you have or less than 1 life.");
                    }

                    if (data.getLives() == 1) {
                        p.sendMessage(
                                ChatColor.YELLOW + String.format("You have %d more life remaining.", data.getLives()));
                    } else {
                        p.sendMessage(
                                ChatColor.YELLOW + String.format("You have %d more lives remaining.", data.getLives()));
                    }
                } else {
                    sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                }
                return true;
            } else if (args.length == 4 && args[0].equalsIgnoreCase("lives") && args[1].equalsIgnoreCase("give")) {
                if (!configManager.getConfig().allowGivingLives()) {
                    sender.sendMessage(ChatColor.DARK_RED + "This command is disabled.");
                    sender.sendMessage(
                            ChatColor.YELLOW + "If you are an admin and this is incorrect, check your config values.");
                    return true;
                }
                if (!(sender instanceof Player)) {
                    sender.sendMessage(ChatColor.RED + "You must be a player.");
                    return true;
                }
                if (PermissionUtil.hasPermission(sender, "hardcorelives.lives.give")) {
                    Player p = (Player) sender;
                    Player target = Bukkit.getPlayer(args[2]);

                    ConfigManager.PlayerData data = configManager.getPlayerData(p);
                    ConfigManager.PlayerData dataTarget = configManager.getPlayerData(target);

                    if (target == null) {
                        sender.sendMessage(ChatColor.RED + "Player not found.");
                        return true;
                    }

                    int numLives = Integer.parseInt(args[3]);

                    if (numLives < data.getLives() && numLives > 0) {
                        data.setLives(data.getLives() - numLives);
                        getServer().getPluginManager().callEvent(new PlayerLifeCountChangedEvent(p, data.getLives()));
                        dataTarget.setLives(dataTarget.getLives() + numLives);
                        getServer().getPluginManager()
                                .callEvent(new PlayerLifeCountChangedEvent(target, dataTarget.getLives()));
                        p.sendMessage(
                                ChatColor.GREEN + String.format("Sent %d lives to %s.", numLives, target.getName()));
                        target.sendMessage(
                                ChatColor.GREEN + String.format("%s sent you %d extra lives.", p.getName(), numLives));
                        updateScoreboard();
                    } else {
                        sender.sendMessage("You cannot give more lives than you have or less than 1 life.");
                    }

                    if (data.getLives() == 1) {
                        p.sendMessage(
                                ChatColor.YELLOW + String.format("You have %d more life remaining.", data.getLives()));
                    } else {
                        p.sendMessage(
                                ChatColor.YELLOW + String.format("You have %d more lives remaining.", data.getLives()));
                    }
                } else {
                    sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                }
                return true;
            } else if (args.length == 2 && args[0].equalsIgnoreCase("scoreboardslot")) {
                if (PermissionUtil.hasPermission(sender, "hardcorelives.scoreboard.manage")) {
                    try {
                        DisplaySlot ds = DisplaySlot.valueOf(args[1]);
                        configManager.getConfig().setScoreboardDisplaySlot(ds);
                        this.scoreboardObjective.setDisplaySlot(ds);
                        updateScoreboard();
                    } catch (IllegalArgumentException e) {
                        sender.sendMessage(ChatColor.RED + "Invalid display slot type.");
                        return true;
                    }
                } else {
                    sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                }
                return true;
            } else {
                sender.sendMessage(ChatColor.RED + "Invalid subcommand. " + ChatColor.AQUA + "/hl" + ChatColor.RED
                        + " for help.");
            }
        }*/
        return super.onCommand(sender, command, label, args);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        log.log(Level.INFO, "Saving config...");
        try {
            configManager.saveConfig(configFile);
        } catch (IOException e) {
            log.severe(ChatColor.RED + "Failed to save config!");
            e.printStackTrace();
        }
        log.log(Level.INFO, "Saving playerdata...");
        try {
            configManager.unloadAllPlayers(playersDir);
        } catch (IOException e) {
            log.severe(ChatColor.RED + "Failed to save playerdata!");
            e.printStackTrace();
        }
        log.log(Level.INFO, "Hardcore Lives disabled.");
    }

    @SuppressWarnings("DuplicatedCode")
    @Override
    public void onEnable() {
        configManager.init(this);

        log.log(Level.INFO, "Loading config...");

        try {
            if (!Files.isDirectory(dataFolder.toPath())) {
                Files.createDirectory(dataFolder.toPath());
            }

            File configFile = new File(Paths.get(dataFolder.toString(), "config.json").toString());
            configManager.loadConfig(configFile);
            playersDir.mkdir();
        } catch (IOException e) {
            log.severe(ChatColor.RED + "Could not create data directory! Will use default config values!");
            e.printStackTrace();
        }

        if (this.getServer().isHardcore()) {
            log.warning("The server is in hardcore mode. Players will be banned on death " +
                    "regardless of this plugin!\nPlease disable hardcore mode in server.properties to allow Lives to " +
                    "work.");
        }

        if (setupEconomy()) {
            log.info("Vault API Hooked.");
        } else {
            log.warning("Vault not found, features that require economy will not work.");
        }

        if (setupPermissions()) {
            PermissionUtil.setPermissionManager(perms);
        }

        PermissionUtil.registerPermissions();

        this.scoreboard = setupScoreboard();

        for (Player p : this.getServer().getOnlinePlayers()) {
            try {
                configManager.loadPlayerData(p, playersDir);
            } catch (IOException e) {
                log.severe(String.format(ChatColor.RED + "Failed to load player %s (UUID %s)!", p.getName(),
                        p.getUniqueId().toString()));
                e.printStackTrace();
            }
        }

        updateScoreboard();

        if (!enabledBefore) {
            this.getServer().getPluginManager().registerEvents(eventHandler, this);
        }

        PluginBaseCommand pbc = new PluginBaseCommand("hardcorelives", this);

        PluginCommand c = this.getCommand("hardcorelives");

        if (c == null) {
            log.severe("Cannot bind plugin command!");
            throw new IllegalStateException();
        }

        c.setExecutor(pbc);
        c.setTabCompleter(pbc);

        saveTaskScheduler.setSaveInterval(configManager.getConfig().getAutosaveUnit()
                .toMillis(configManager.getConfig().getAutosaveInterval()) / 50);
        saveTaskScheduler.rescheduleTask();

        this.enabledBefore = true;
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return true;
    }

    private boolean setupPermissions() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        if (rsp == null) {
            return false;
        }
        perms = rsp.getProvider();
        return true;
    }

    public void updateScoreboard() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            updateScoreboard(p);
        }
    }

    private Scoreboard setupScoreboard() {
        Scoreboard livesScoreboard = getServer().getScoreboardManager().getNewScoreboard();

        this.scoreboardObjective = livesScoreboard.registerNewObjective("lives", "dummy", "Lives");
        this.scoreboardObjective.setDisplayName("Lives");
        this.scoreboardObjective.setDisplaySlot(configManager.getConfig().getScoreboardDisplaySlot());

        return livesScoreboard;
    }

    public void updateScoreboard(Player toUpdate) {
        Score playerLivesScore = this.scoreboardObjective.getScore(toUpdate.getName());
        playerLivesScore.setScore(configManager.getPlayerData(toUpdate).getLives());
    }

    @SuppressWarnings("DuplicatedCode")
    public void reload(boolean saveConfig) {
        log.log(Level.INFO, "Reloading HardcoreLives...");

        if (saveConfig) {
            log.log(Level.INFO, "Saving config...");
            try {
                configManager.saveConfig(configFile);
            } catch (IOException e) {
                log.severe(ChatColor.RED + "Failed to save config!");
                e.printStackTrace();
            }
        }
        log.log(Level.INFO, "Saving playerdata...");
        try {
            configManager.unloadAllPlayers(playersDir);
        } catch (IOException e) {
            log.severe(ChatColor.RED + "Failed to save playerdata!");
            e.printStackTrace();
        }

        log.log(Level.INFO, "Hardcore Lives disabled.");

        configManager.init(this);

        log.log(Level.INFO, "Loading config...");

        try {
            if (!Files.isDirectory(dataFolder.toPath())) {
                Files.createDirectory(dataFolder.toPath());
            }

            File configFile = new File(Paths.get(dataFolder.toString(), "config.json").toString());
            configManager.loadConfig(configFile);
            playersDir.mkdir();
        } catch (IOException e) {
            log.severe(ChatColor.RED + "Could not create data directory! Will use default config values!");
            e.printStackTrace();
        }

        if (this.getServer().isHardcore()) {
            log.warning("The server is in hardcore mode. Players will be banned on death " +
                    "regardless of this plugin!\nPlease disable hardcore mode in server.properties to allow Lives to " +
                    "work.");
        }

        if (setupEconomy()) {
            log.info("Vault API Hooked.");
        } else {
            log.warning("Vault not found, features that require economy will not work.");
        }

        this.scoreboard = setupScoreboard();

        for (Player p : this.getServer().getOnlinePlayers()) {
            try {
                configManager.loadPlayerData(p, playersDir);
            } catch (IOException e) {
                log.severe(String.format(ChatColor.RED + "Failed to load player %s (UUID %s)!", p.getName(),
                        p.getUniqueId().toString()));
                e.printStackTrace();
            }
        }

        updateScoreboard();
    }

    public Scoreboard getScoreboard() {
        return scoreboard;
    }

    public Objective getScoreboardObjective() {
        return scoreboardObjective;
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }

    public SaveTaskScheduler getSaveTaskScheduler() {
        return saveTaskScheduler;
    }

    public File getPlayersDir() {
        return playersDir;
    }

    public File getConfigFile() {
        return configFile;
    }
}
