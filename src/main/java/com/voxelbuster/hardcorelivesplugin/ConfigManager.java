package com.voxelbuster.hardcorelivesplugin;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;

import java.io.*;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class ConfigManager {
    private Config config;
    private HashMap<Player, PlayerData> playerDataMap = new HashMap<>();
    private HardcoreLivesPlugin plugin;

    public void init(HardcoreLivesPlugin plugin) {
        this.plugin = plugin;
    }

    public boolean loadConfig(File configFile) throws IOException {
        if (generateConfig(configFile)) {
            return true;
        } else {
            Gson gson = new Gson();
            FileReader fr = new FileReader(configFile);
            try {
                this.config = gson.fromJson(fr, Config.class);
            } catch (JsonSyntaxException e) {
                plugin.getLogger().severe("Bad JSON in config, resetting.");
                e.printStackTrace();
                generateConfig(configFile);
                return false;
            }
            return false;
        }
    }

    public boolean generateConfig(File configFile) throws IOException {
        if (!configFile.exists()) {
            configFile.createNewFile();
            FileWriter fw = new FileWriter(configFile);
            this.config = new Config();
            Gson gson = new Gson();
            gson.toJson(config, fw);
            fw.close();
            return true;
        }

        return false;
    }

    public void saveConfig(File configFile) throws IOException {
        FileWriter fw = new FileWriter(configFile);
        Gson gson = new Gson();
        gson.toJson(config, fw);
        fw.close();
    }

    public void setPlayerData(Player p, PlayerData data) {
        this.playerDataMap.put(p, data);
    }

    public void saveAllPlayers(File playersDir) throws IOException {
        for (Player p : playerDataMap.keySet()) {
            savePlayer(p, playersDir);
        }
    }

    public boolean savePlayer(Player p, File playersDir) throws IOException {
        if (!playerDataMap.containsKey(p)) {
            return false;
        } else {
            Gson gson = new Gson();
            FileWriter fw = new FileWriter(
                    Paths.get(playersDir.toString(), p.getUniqueId().toString() + ".json").toString());
            gson.toJson(playerDataMap.get(p), fw);
            fw.close();
            return true;
        }
    }

    public void wipePlayers(File playersDir) throws IOException {
        Player[] loadedPlayers = getLoadedPlayers();
        unloadAllPlayers(playersDir);
        File oldPlayers = new File(playersDir.toPath().toString());
        oldPlayers.renameTo(new File(Paths.get(playersDir.getParentFile().toPath().toString(),
                "players.old").toString()));
        playersDir.mkdir();
        playerDataMap.clear();
        for (Player p : loadedPlayers) {
            loadPlayerData(p, playersDir);
        }
    }

    public Player[] getLoadedPlayers() {
        return playerDataMap.keySet().toArray(new Player[0]);
    }

    public void unloadAllPlayers(File playersDir) throws IOException {
        for (Player p : playerDataMap.keySet()) {
            unloadPlayer(p, playersDir);
        }
    }

    /**
     * Loads the PlayerData into the internal Map. Returns true if the file was created and false if it already exists.
     *
     * @param p
     * @param playersDir
     * @return
     * @throws IOException
     */
    public boolean loadPlayerData(Player p, File playersDir) throws IOException {
        if (!playersDir.isDirectory()) {
            throw new FileNotFoundException("playersDir must be a directory!");
        } else {
            File playerFile = new File(
                    Paths.get(playersDir.toString(), p.getUniqueId().toString() + ".json").toString());
            if (!playerFile.exists()) {
                playerFile.createNewFile();
                PlayerData playerData = new PlayerData(p, this.getConfig());
                playerDataMap.put(p, playerData);
                return true;
            } else {
                Gson gson = new Gson();
                FileReader fr = new FileReader(playerFile);
                try {
                    PlayerData pd = gson.fromJson(fr, PlayerData.class);
                    playerDataMap.put(p, pd);
                } catch (JsonSyntaxException e) {
                    plugin.getLogger().severe("Bad JSON in config, resetting.");
                    e.printStackTrace();
                    playerDataMap.put(p, new PlayerData(p, this.getConfig()));
                }
                fr.close();
                return false;
            }
        }
    }

    public boolean unloadPlayer(Player p, File playersDir) throws IOException {
        if (!playerDataMap.containsKey(p)) {
            return false;
        } else {
            Gson gson = new Gson();
            FileWriter fw = new FileWriter(
                    Paths.get(playersDir.toString(), p.getUniqueId().toString() + ".json").toString());
            gson.toJson(playerDataMap.remove(p), fw);
            fw.close();
            return true;
        }
    }

    public Config getConfig() {
        return config;
    }

    public PlayerData getPlayerData(Player p) {
        return playerDataMap.get(p);
    }

    public static class Config implements Serializable {
        private int startingLives = 3;
        private float lifeBuyPrice = 10000;
        private float lifeSellPrice = 5000;
        private int autosaveInterval = 5;
        private TimeUnit autosaveUnit = TimeUnit.MINUTES;
        private boolean allowBuyingLives = false;
        private boolean allowSellingLives = false;
        private boolean allowGivingLives = false;
        private boolean allowTotemOfUndying = true;
        private boolean spectateWhenNoMoreLives = true;
        private DisplaySlot scoreboardDisplaySlot = DisplaySlot.BELOW_NAME;

        public int getStartingLives() {
            return startingLives;
        }

        public void setStartingLives(int startingLives) {
            this.startingLives = startingLives;
        }

        public boolean allowsTotemOfUndying() {
            return allowTotemOfUndying;
        }

        public void setAllowTotemOfUndying(boolean allowTotemOfUndying) {
            this.allowTotemOfUndying = allowTotemOfUndying;
        }

        public boolean canSpectateWhenNoMoreLives() {
            return spectateWhenNoMoreLives;
        }

        public void setSpectateWhenNoMoreLives(boolean spectateWhenNoMoreLives) {
            this.spectateWhenNoMoreLives = spectateWhenNoMoreLives;
        }

        public float getLifeBuyPrice() {
            return lifeBuyPrice;
        }

        public void setLifeBuyPrice(float lifeBuyPrice) {
            this.lifeBuyPrice = lifeBuyPrice;
        }

        public float getLifeSellPrice() {
            return lifeSellPrice;
        }

        public void setLifeSellPrice(float lifeSellPrice) {
            this.lifeSellPrice = lifeSellPrice;
        }

        public boolean allowBuyingLives() {
            return allowBuyingLives;
        }

        public void setAllowBuyingLives(boolean allowBuyingLives) {
            this.allowBuyingLives = allowBuyingLives;
        }

        public boolean allowSellingLives() {
            return allowSellingLives;
        }

        public void setAllowSellingLives(boolean allowSellingLives) {
            this.allowSellingLives = allowSellingLives;
        }

        public boolean allowGivingLives() {
            return allowGivingLives;
        }

        public void setAllowGivingLives(boolean allowGivingLives) {
            this.allowGivingLives = allowGivingLives;
        }

        public DisplaySlot getScoreboardDisplaySlot() {
            return this.scoreboardDisplaySlot;
        }

        public void setScoreboardDisplaySlot(DisplaySlot scoreboardDisplaySlot) {
            this.scoreboardDisplaySlot = scoreboardDisplaySlot;
        }

        public int getAutosaveInterval() {
            return autosaveInterval;
        }

        public void setAutosaveInterval(int autosaveInterval) {
            this.autosaveInterval = autosaveInterval;
        }

        public TimeUnit getAutosaveUnit() {
            return autosaveUnit;
        }

        public void setAutosaveUnit(TimeUnit autosaveUnit) {
            this.autosaveUnit = autosaveUnit;
        }
    }

    public static class PlayerData implements Serializable {
        private UUID uuid;
        private String username;
        private int lives;
        private boolean bypassLives;

        public PlayerData(Player p, Config c) {
            this(p, c.getStartingLives(), false);
        }

        public PlayerData(Player p, int lives, boolean bypassLives) {
            this.uuid = p.getUniqueId();
            this.username = p.getName();
            this.bypassLives = bypassLives;
            this.lives = lives;
        }

        public UUID getUuid() {
            return uuid;
        }

        public void setUuid(UUID uuid) {
            this.uuid = uuid;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public int getLives() {
            return lives;
        }

        public void setLives(int lives) {
            this.lives = lives;
        }

        public boolean doesBypassLives() {
            return bypassLives;
        }

        public void setBypassLives(boolean bypassLives) {
            this.bypassLives = bypassLives;
        }
    }
}
